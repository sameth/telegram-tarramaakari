const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const image = document.getElementById('source');
const widthSlider = document.getElementById('widthSlider');
const heightSlider = document.getElementById('heightSlider');
const sticker = {
    w: image.width,
    h: image.height,
    x: 0,
    y: 0,
    filename: image.filename
}

function initImage() {
    var initValue = 250;
    setWidth(initValue);
    setHeight(initValue);
    widthValue.innerHTML = initValue;
    heightValue.innerHTML = initValue;
    widthSlider.value = initValue;
    heightSlider.value = initValue;
}

function drawImage() {
    ctx.drawImage(image, sticker.x, sticker.y, sticker.w, sticker.h);
}

function update() {
    clear();
    drawImage();
}

function setWidth(width) {
    sticker.w = width;
    update();
}

function setHeight(height) {
    sticker.h = height;
    update();
}

function clear() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}
widthSlider.oninput = function() {
    setWidth(this.value);
    widthValue.innerHTML = this.value;
}
heightSlider.oninput = function() {
    setHeight(this.value);
    heightValue.innerHTML = this.value;
}

function loadFile() {
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();
    reader.addEventListener("load", function() {
        // convert image file to base64 string
        image.setAttribute('crossorigin', 'anonymous');
        image.src = reader.result;
    }, false);
    reader.addEventListener("loadend", update); // Update image when its done loading
    if (file) {
        reader.readAsDataURL(file);
        sticker.filename = file.name;
    }
}

function download() {
    const oldFilename = sticker.filename;
    const filenameWithoutExtension = oldFilename.split('.').slice(0, -1).join('.');
    const newFilename = filenameWithoutExtension + "-" + sticker.w + "x" + sticker.h + ".png";
    resizeCanvasToImageSize();
    var download = document.getElementById("download");
    var image = document.getElementById("canvas").toDataURL("image/png")
        .replace("image/png", "image/octet-stream");
    download.setAttribute("href", image);
    download.setAttribute("download", newFilename);
    restoreCanvasSize();
}

function resizeCanvasToImageSize() {
    // Resize canvas to image dimensions for download
    canvas.width = sticker.w;
    canvas.height = sticker.h;
    loadFile();
    update();
}

function restoreCanvasSize() {
    // Restore canvas back to 512x512
    canvas.width = 512;
    canvas.height = 512;
    loadFile();
    update();
}
initImage();