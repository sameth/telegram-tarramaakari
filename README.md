# Telegram Tarramaakari

## Hosted at [same.bugi.blanko.fi/tarramaakari/](https://same.bugi.blanko.fi/tarramaakari/)


## What

### Telegram sticker resizer

- Import your own image and make it the right size easily
- Works on mobile too
- Everything is client side, nothing gets uploaded to the server


## How

### Javascript
- Upload file to the editor
- Make sure the other side is 512px, resize however you want
- Press the magic download button

## Why

### Why not?
- Why do something in a few mins, when you can spend 5 hours automating it!
- I make a lot of stickers